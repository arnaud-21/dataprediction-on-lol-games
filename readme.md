The dataset is provided by michel's fanboi and available on :  
https://www.kaggle.com/bobbyscience/league-of-legends-diamond-ranked-games-10-min  
Information such as Context and explanation of the dataset are found on the link above.  

Glossary

    Warding totem: An item that a player can put on the map to reveal the nearby area. Very useful for map/objectives control.
    Minions: NPC that belong to both teams. They give gold when killed by players.
    Jungle minions: NPC that belong to NO TEAM. They give gold and buffs when killed by players.
    Elite monsters: Monsters with high hp/damage that give a massive bonus (gold/XP/stats) when killed by a team.
    Dragons: Elite monster which gives team bonus when killed. The 4th dragon killed by a team gives a massive stats bonus. The 5th dragon (Elder Dragon) offers a huge advantage to the team.
    Herald: Elite monster which gives stats bonus when killed by the player. It helps to push a lane and destroys structures.
    Towers: Structures you have to destroy to reach the enemy Nexus. They give gold.
    Level: Champion level. Start at 1. Max is 18.

